<?php

namespace App\Model\User;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use App\Events\UserRegistered;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    public function findForPassport($username) {
        return $this->where('username', $username)->first();
    }

    protected $fillable = [
        'id', 'firstname','middlename','surname', 'email', 'password', 'username','role','parent_id'
    ];

   
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function devices()
    {
        return $this->hasMany('App\Model\Device\Device', 'user_id');
    }

    public function organizationMember()
    {
        return $this->hasOne('App\Model\Organization\OrganizationMember', 'user_id');
    }


    public function logs(){
        return $this->hasMany('App\Model\User\User', 'parent_id', 'id' );
    }

  
    protected static function boot() {
        parent::boot();

        static::creating(function($user) { 
            
             $user->email_token =bin2hex(random_bytes(32)); 
             
        });

        static::created(function($user){
            event(new UserRegistered($user));
        });

        static::deleting(function($user) {
            if($user->organizationMember)
            $user->organizationMember->delete();
        });
    }
}
