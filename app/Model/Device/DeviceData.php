<?php

namespace App\Model\Device;

use Illuminate\Database\Eloquent\Model;
use App\Model\User\User;

class DeviceData extends Model
{

   protected $table = 'device_data';
   protected $fillable = ['data','device_id','property_id','latitude','longitude','value','date'];

   public function device()
    {
        return $this->belongsTo('App\Model\Device\Device', 'device_id');
	}

	public function property()
  {
      return $this->belongsTo('App\Model\Device\Property', 'property_id');
  }


}
