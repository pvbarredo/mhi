<?php

namespace App\Model\Device;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{

    protected $table = 'device';
    protected $fillable = ['name','username','mac_address','description', 'user_id', 'type_id','status'];


    public function owner()
    {
        return $this->belongsTo('App\Model\User\User', 'user_id');
	}

	public function type()
    {
        return $this->belongsTo('App\Model\Device\Type', 'type_id');
    }

    public function deviceData()
    {
        return $this->hasMany('App\Model\Device\DeviceData', 'device_id');
    }

    
    protected static function boot() {
        parent::boot();
        static::deleting(function($device) {
            foreach ($device->deviceData as $deviceData)
            {
                $deviceData->delete();
            }
        });

       
    }

}
