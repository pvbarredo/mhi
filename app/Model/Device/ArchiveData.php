<?php

namespace App\Model\Device;

use Illuminate\Database\Eloquent\Model;
use App\Model\User\User;

class ArchiveData extends Model
{

   protected $table = 'archive_data';
   protected $fillable = ['device_id','property_id','latitude','longitude','value','date'];

   public function device()
    {
        return $this->belongsTo('App\Model\Device\Device', 'device_id');
	}

	public function property()
  {
      return $this->belongsTo('App\Model\Device\Property', 'property_id');
  }

}
