<?php

namespace App\Model\Device;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'device_type';
    protected $fillable = ['code','name'];
    
    public function device()
    {
        return $this->hasMany('App\Model\Device\Device', 'type_id');
	}

	public function property()
    {
        return $this->hasMany('App\Model\Device\Property', 'type_id');
	}

    protected static function boot() {
        parent::boot();
        static::deleting(function($type) { 
            foreach ($type->device as $device){
                $device->delete();
            }

            foreach ($type->property as $property){
                $property->delete();
            }
        });
    }

}
