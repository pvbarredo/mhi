<?php

namespace App\Model\Organization;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table = 'organization';
    protected $fillable = ['name'];
    
    public function members()
    {
        return $this->hasMany('App\Model\Organization\OrganizationMember', 'organization_id');
    }
    
    protected static function boot() {
        parent::boot();

        static::deleting(function($organization) {
            if($organization->members){
                foreach ($organization->members as $member)
                {
                    $member->delete();
                }
            }
        });
    }


}
