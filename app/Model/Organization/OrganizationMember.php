<?php

namespace App\Model\Organization;

use Illuminate\Database\Eloquent\Model;

class OrganizationMember extends Model
{
    protected $table = 'organization_member';
    protected $fillable = ['organization_id','user_id','approved'];
    
    public function organization()
    {
        return $this->belongsTo('App\Model\Organization\Organization', 'organization_id');
	}

	public function user()
    {
         return $this->belongsTo('App\Model\User\User', 'user_id');
	}

}
