<?php

namespace App\Http\Controllers\Device;

use App\Http\Controllers\Controller;
use App\Model\Device\ArchiveData;
use App\Model\Device\Device;
use App\Model\Device\DeviceData;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class DeviceDataController extends Controller
{
    public function logActivity($model, $id)
    {
        activity('create-device-data')
            ->performedOn($model)
            ->causedBy(User::find($id))
            ->withProperties([
                'device_id' => $model->device_id,
                'property_id' => $model->property_id,
                'value' => $model->value,
                'latitude' => $model->latitude,
                'longitude' => $model->longitude,
            ])
            ->log('Created Device data by Seeder');

    }

    public function index(Request $request)
    {
        $data = DeviceData::with('device.type')->with('property.criticalValue')->latest();
        $data = self::criticalCheck($data->get());
        return self::manualPaginate($data, ((int) $request->input('page')));
    }

    public function search(Request $request)
    {
        $data = (new DeviceData)->newQuery();
        $data->with('device')->with('property')->with('property.criticalValue')->with('device.type');

        if ($request->input('property')) {
            $data->whereHas('property', function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->input('property') . '%');
            });
        }
        if ($request->input('device_id')) {
            $data->where('device_id', $request->input('device_id'));
        }

        if ($request->input('type')) {
            $data->whereHas('device.type', function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->input('type') . '%');
            });
        }

        if ($request->input('device')) {
            $data->whereHas('device', function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->input('device') . '%');
            });
        }

        $data = self::criticalCheck($data->get());

        if ($request->input('status') && $request->input('status') != 'All') {
            $status = $request->input('status');
            $filteredData = [];

            if ($status == 'Critical') {
                foreach ($data as $data) {
                    if ($data['status'] == 1) {
                        array_push($filteredData, $data);
                    }

                }
            } else {
                foreach ($data as $data) {
                    if ($data['status'] == 0) {
                        array_push($filteredData, $data);
                    }

                }
            }
            $data = $filteredData;
        }

        return self::manualPaginate($data, ((int) $request->input('page')));
    }

    public function getAllDeviceData()
    {
        $array_devices = [];
        $devices = Device::with('type.property')->get();

        foreach ($devices as $device) {
            $deviceData = DeviceData::with('property.criticalValue')->with('device.type')->where('device_id', '=', $device->id)->orderBy('date')->get();

            $device->deviceData = $deviceData;

            if (count($deviceData)) {
                array_push($array_devices, $device);
            }

        }
        // return DeviceData::with('device.type')->with('property.criticalValue')->latest()->get();
        return response()->json($array_devices);
    }

    public function dialogGetData(Request $request)
    {
        $device_id = $request->get('device_id');
        $data = DeviceData::with('device.type')->with('property.criticalValue')->where('device_id', $device_id);
        $data = self::criticalCheck($data->get());

        return self::manualPaginate($data, ((int) $request->input('page')));
    }

    private function manualPaginate($datas, $page)
    {
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;
        $total = count($datas);
        $datas = array_slice($datas, $offset, $perPage);

        $paginator = new LengthAwarePaginator(
            $datas,
            $total,
            $perPage,
            $page
        );

        return $paginator;
    }

    private function criticalCheck($datas)
    {
        $list = $datas;
        foreach ($list as $data) {
            $criticalValues = $data->property->criticalvalue;
            $value = $data->value;
            $criticalMessages = [];

            foreach ($criticalValues as $criticalValue) {
                switch ($criticalValue->condition) {
                    case 'Exact':
                        if ($criticalValue->min_value == $value) {
                            array_push($criticalMessages, $criticalValue->description);
                        }
                        break;
                    case 'Range':
                        if ($criticalValue->min_value <= $value && $criticalValue->max_value >= $value) {
                            array_push($criticalMessages, $criticalValue->description);
                        }
                        break;

                    case 'Above':
                        if ($criticalValue->min_value <= $value) {
                            array_push($criticalMessages, $criticalValue->description);
                        }
                        break;
                    case 'Below':
                        if ($criticalValue->min_value >= $value) {
                            array_push($criticalMessages, $criticalValue->description);
                        }
                        break;
                }
            }
            $data->criticalMessages = $criticalMessages;
            $data->status = 0;
            if (count($criticalMessages) > 0) {
                $data->status = 1;
            }
        }

        return $list->toArray();
    }

    public function store(Request $request)
    {
        return response()->json(
            Device::create(
                [
                    'name' => $request->get('name'),
                    'username' => $request->get('username'),
                    'mac_address' => $request->get('mac_address'),
                    'password' => bcrypt($request->get('password')),
                ]
            )
        );
    }

    public function show($id)
    {
        return response()->json(Device::find($id));
    }
    public function getDataUsingPropertyId($id)
    {
        return DeviceData::where('property_id', $id)->get();

    }

    public function addDeviceData(Request $request)
    {

        if (!$request->get('value') || !$request->get('latitude') || !$request->get('longitude')) {
            return response()->json(ArchiveData::create(
                [
                    'device_id' => $request->get('device_id'),
                    'property_id' => $request->get('property_id'),
                    'value' => $request->get('value'),
                    'latitude' => $request->get('latitude'),
                    'longitude' => $request->get('longitude'),
                    'date' => ($request->get('date')) ? date($request->get('date') . ' ' . $request->get('time')) : null,
                ]
            )
            );
        }

        return response()->json(
            DeviceData::create(
                [
                    'device_id' => $request->get('device_id'),
                    'property_id' => $request->get('property_id'),
                    'value' => $request->get('value'),
                    'latitude' => $request->get('latitude'),
                    'longitude' => $request->get('longitude'),
                    'date' => date($request->get('date') . ' ' . $request->get('time')),
                ]
            )
        );
    }

}
