<?php

namespace App\Http\Controllers\Device;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Device\ArchiveData;
use App\Model\Device\Device;
use App\Model\Device\Type;
use Illuminate\Pagination\LengthAwarePaginator;

class ArchiveDataController extends Controller
{
    public function logActivity( $model, $id){
        activity('create-archive-data')
           ->performedOn($model)
           ->causedBy(User::find($id))
           ->withProperties([
                'device_id'=> $model->device_id,
                'property_id' => $model->property_id,
                'value' => $model->value,
                'latitude' => $model->latitude,
                'longitude' => $model->longitude
            ])
           ->log('Created Archive data by Seeder');

    }
    
    public function index(Request $request)
    {
        $data = ArchiveData::with('device.type')->with('property.criticalValue')->latest();
        $data = self::criticalCheck($data->get());
        return self::manualPaginate($data, ((int)$request->input('page')));
    }

    public function search(Request $request)
    {
        $data = (new ArchiveData)->newQuery();
        $data->with('device')->with('property')->with('property.criticalValue')->with('device.type');
        
        if($request->input('archive')){  
            $data->whereHas('device', function($query) use ($request) {
                $query->where('name','like', '%' . $request->input('archive'). '%');
            });
        }
        
        if($request->input('property')) {  
            $data->whereHas('property', function($query) use ($request) {
                $query->where('name','like', '%' . $request->input('property'). '%');
            });
        }
        
        if($request->input('type')){  
            $data->whereHas('device.type', function($query) use ($request) {
                $query->where('name','like', '%' . $request->input('type'). '%');
            });
        }
        
        $data = self::criticalCheck($data->get());

        if($request->input('status') && $request->input('status')!='All') {  
            $status = $request->input('status');
            $filteredData = [];
    
            if($status == 'Critical'){
                foreach($data as $data){
                    if($data['status'] == 1)
                    array_push($filteredData,$data);
                }
            }
            else{
                foreach($data as $data){
                    if($data['status'] == 0)
                    array_push($filteredData,$data);
                }
            }
            $data = $filteredData;
        }

        return self::manualPaginate($data, ((int)$request->input('page')));
    }

    public function getAllArchiveData()
    {
        $array_devices =[];
        $devices = Device::with('type')->get();

        foreach ($devices as $device) {
            $deviceData = ArchiveData::with('property.criticalValue')->where('device_id', '=', $device->id)->orderBy('date')->get();

            $device->deviceData = $deviceData;

            if(count($deviceData)) {
                array_push($array_devices, $device);    
            }
            
        }
        // return ArchiveData::with('device.type')->with('property.criticalValue')->latest()->get();   
        return response()->json($array_devices);
    }
    
    public function getArchiveType()
    {
        $archive = ArchiveData::has('device.type')->distinct()->select('property_id')->get();
        $type = Type::distinct()->select('name')->whereIn('id', $archive)->get();
        return $type;
    }

    private function manualPaginate($datas, $page){
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;
        $total = count($datas);
        $datas = array_slice($datas, $offset, $perPage);

        $paginator = new LengthAwarePaginator(
            $datas,
            $total,
            $perPage,
            $page
        );
        
        return $paginator;
    }

    private function criticalCheck($datas){
        $list = $datas;
        foreach($list as $data){
            $criticalValues = $data->property->criticalvalue;
            $value = $data->value;
            $criticalMessages = [];

            foreach($criticalValues as $criticalValue){
                switch($criticalValue->condition){
                    case 'Exact':
                        if ($criticalValue->min_value == $value) {
                            array_push($criticalMessages,$criticalValue->description);
                        }
                        break;
                    case 'Range':
                        if ($criticalValue->min_value <= $value && $criticalValue->max_value >= $value) {
                            array_push($criticalMessages,$criticalValue->description);
                        }
                        break;

                    case 'Above':
                        if ($criticalValue->min_value <= $value) {
                            array_push($criticalMessages,$criticalValue->description);
                        }
                        break;
                    case 'Below':
                        if ($criticalValue->min_value >= $value) {
                            array_push($criticalMessages,$criticalValue->description);
                        }
                        break;
                }
            }
            $data->criticalMessages = $criticalMessages;
            $data->status = 0;
            if (count($criticalMessages)>0) {
                $data->status = 1;
            }
        }
        
        return $list->toArray();
    }

    public function store(Request $request)
    {
        return response()->json(
            Device::create(
                [
                'name' => $request->get('name'),
                'username' => $request->get('username'),
                'mac_address' => $request->get('mac_address'),
                'password' => bcrypt($request->get('password')),
                ]
            ) 
        );
    }

    public function show($id)
    {
        return response()->json(Device::find($id));
    }

    public function deleteAllData()
    {
        ArchiveData::truncate();

        return 'Success';
    }



    public function addArchiveData(Request $request)
    {
        return response()->json(
            ArchiveData::create(
                [
                'device_id' => $request->get('device_id'),
                'property_id' => $request->get('property_id'),
                'value' => $request->get('value'),
                'latitude' => $request->get('latitude'),
                'longitude' =>$request->get('longitude'),
                'date' => date($request->get('date') . ' ' . $request->get('time'))
                ]
            ) 
        );
    }



}
