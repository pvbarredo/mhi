<?php

namespace App\Http\Controllers\Device;

use App\Http\Controllers\Controller;
use App\Model\Device\CriticalValue;
use App\Model\Device\Device;
use App\Model\Device\Property;
use App\Model\Device\Type;
use App\Model\User\User;
use DB;
use Illuminate\Http\Request;
use Storage;

class DeviceController extends Controller
{
    public function logActivityDevice($model, $causer, $activity, $action)
    {
        //message format is '{{causer}} {{action}} {{performedOn}}'
        $message = $causer->username . ' ' . $action . ' ' . 'device: ' . ('"' . $model->username . '"');

        activity($activity)
            ->performedOn($model)
            ->causedBy($causer)
            ->withProperties([
                'name' => $model->name,
                'username' => $model->username,
                'mac_address' => $model->mac_address,
                'user_id' => $model->user_id,
                'type_id' => $model->type_id,
                'description' => $model->description,
            ])
            ->log($message);

    }

    public function logActivityType($model, $causer, $activity, $action)
    {
        //message format is '{{causer}} {{action}} {{performedOn}}'
        $message = $causer->username . ' ' . $action . ' ' . 'type: ' . ('"' . $model->name . '"');

        activity($activity)
            ->performedOn($model)
            ->causedBy($causer)
            ->withProperties([
                'code' => $model->code,
                'name' => $model->name,
            ])
            ->log($message);

    }

    public function logActivityCriticalValue($model, $causer, $activity, $action)
    {
        //message format is '{{causer}} {{action}} {{performedOn}}'
        $message = $causer->username . ' ' . $action . ' ' . 'critical value';

        activity($activity)
            ->performedOn($model)
            ->causedBy($causer)
            ->withProperties([
                'property_id' => $model->property_id,
                'min_value' => $model->min_value,
                'max_value' => $model->max_value,
                'description' => $model->description,
                'condition' => $model->condition,
            ])
            ->log($message);

    }

    public function logActivityProperty($model, $causer, $activity, $action)
    {
        //message format is '{{causer}} {{action}} {{performedOn}}'
        $message = $causer->username . ' ' . $action . ' ' . 'property: ' . ('"' . $model->name . '"');

        activity($activity)
            ->performedOn($model)
            ->causedBy($causer)
            ->withProperties([
                'code' => $model->code,
                'name' => $model->name,
                'unit' => $model->unit,
                'type_id' => $model->type_id,
            ])
            ->log($message);

    }

    public function index()
    {
        return Device::with('type.property.criticalValue')->with('owner.organizationMember')->paginate(100);
    }

    public function checkUsername(Request $request)
    {

        return Device::where('username', $request->get('username'))->first();

    }

    public function checkMacAddress(Request $request)
    {

        return Device::where('mac_address', $request->get('mac_address'))->first();

    }

    public function checkTypeCode(Request $request)
    {

        return Type::where('code', $request->get('type')["code"])->first();

    }

    public function getAllFreeDevices()
    {
        return Device::with('type.property.criticalValue')->whereNull('user_id')->get();
    }

    public function getAllDevices()
    {
        return Device::with('type.property.criticalValue')->get();
    }

    public function getAllDevicesPost(Request $request)
    {
        $currentUser = $request->get('currentUser');
        dd($currentUser);
        return Device::with('type.property.criticalValue')->with('owner.organizationMember')->get();
    }

    public function getDeviceType()
    {
        return Type::distinct()->select('name')->groupBy('name')->get();
    }

    public function getDeviceProperty()
    {
        return Property::distinct()->select('name')->groupBy('name')->get();
    }

    public function search(Request $request)
    {
        $device = (new Device)->newQuery();
        $device->with('type');

        if ($request->input('name')) {
            $device->where('name', 'like', '%' . $request->input('name') . '%');
        }

        if ($request->input('username')) {
            $device->where('username', 'like', '%' . $request->input('username') . '%');
        }
        if ($request->input('type')) {
            $device->whereHas('type', function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->input('type') . '%');
            });
        }
        if ($request->input('macAddress')) {
            $device->where('mac_address', 'like', '%' . $request->input('macAddress') . '%');
        }

        return $device->paginate(10);
    }

    public function store(Request $request)
    {

        $transaction = DB::transaction(function () use ($request) {
            $type = Type::create([
                'icon' => 'sample',
                'code' => $request->get('device')['type']["code"],
                'name' => $request->get('device')['type']["name"],
            ]);
            $causer = new User($request->get('currentUser'));
            $this->logActivityType($type, $causer, 'create-type', 'created');

            foreach ($request->get('device')['type']["property"] as $property) {
                $prop = Property::create([
                    'code' => $property["code"],
                    'name' => $property["name"],
                    'unit' => $property["unit"],
                    'type_id' => $type->id,
                ]);
                $this->logActivityProperty($prop, $causer, 'create-property', 'created');
                foreach ($property["critical_value"] as $criticalVal) {
                    $crit = CriticalValue::create([
                        'min_value' => $criticalVal["min_value"],
                        'max_value' => $criticalVal["max_value"],
                        'condition' => $criticalVal["condition"],
                        'description' => $criticalVal["description"],
                        'property_id' => $prop->id,
                    ]);
                    $this->logActivityCriticalValue($crit, $causer, 'create-critical-value', 'created');
                }
            }

            $device = Device::create([
                'name' => $request->get('device')['name'],
                'username' => $request->get('device')['username'],
                'mac_address' => $request->get('device')['mac_address'],
                // 'password' => bcrypt($request->get('password')),
                'description' => $request->get('device')['description'],
                'status' => $request->get('device')['status'],
                'user_id' => $request->get('device')['owner']["id"],
                'type_id' => $type->id,
            ]);

            $this->logActivityDevice($device, $causer, 'create-device', 'created');

            //change from p3 to p2
            $user = User::find($request->get('device')['owner']["id"]);
            if ($user->role == "P3") {
                $user->role = "P2";
                $user->save();
            }
            return $device;
        });

        return response()->json([
            'success' => true,
            'device' => $transaction,
        ]);
    }

    public function update(Request $request, $id)
    {

        $transaction = DB::transaction(function () use ($request, $id) {

            $device = Device::find($id);
            $device->name = $request->get('device')['name'];
            $device->username = $request->get('device')['username'];
            $device->mac_address = $request->get('device')['mac_address'];
            $device->description = $request->get('device')['description'];
            $device->status = $request->get('device')['status'];
            $device->user_id = $request->get('device')['owner']["id"];
            $device->type_id = $request->get('device')['type_id'];

            $device->save();

            $causer = new User($request->get('currentUser'));
            $this->logActivityDevice($device, $causer, 'update-device', 'updated');

            $type = Type::with('property')->find($request->get('device')['type_id']);
            $type->code = $request->get('device')['type']["code"];
            $type->name = $request->get('device')['type']["name"];

            $type->save();
            $this->logActivityType($type, $causer, 'update-type', 'updated');

            foreach ($request->get('device')['type']["property"] as $property) {
                if (isset($property["id"])) {
                    $prop = Property::find($property["id"]);
                    $prop->code = $property["code"];
                    $prop->name = $property["name"];
                    $prop->unit = $property["unit"];
                    $prop->type_id = $type->id;

                    $prop->save();
                    $this->logActivityType($prop, $causer, 'update-property', 'updated');

                    foreach ($property["critical_value"] as $crit) {
                        if (isset($crit["id"])) {
                            $critical = CriticalValue::find($crit["id"]);
                            $critical->min_value = $crit["min_value"];
                            $critical->max_value = $crit["max_value"];
                            $critical->condition = $crit["condition"];
                            $critical->description = $crit["description"];
                            $critical->property_id = $crit["property_id"];
                            $critical->save();
                            $this->logActivityCriticalValue($critical, $causer, 'update-critical-value', 'updated');
                        } else {
                            $critical = CriticalValue::create([
                                'min_value' => $crit["min_value"],
                                'max_value' => $crit["max_value"],
                                'condition' => $crit["condition"],
                                'description' => $crit["description"],
                                'property_id' => $prop->id,
                            ]);
                            $this->logActivityCriticalValue($critical, $causer, 'update-critical-value', 'updated');
                        }

                    }

                } else {
                    $prop = Property::create([
                        'code' => $property["code"],
                        'name' => $property["name"],
                        'unit' => $property["unit"],
                        'type_id' => $type->id,
                    ]);
                    foreach ($property["critical_value"] as $crit) {
                        CriticalValue::create([
                            'min_value' => $crit["min_value"],
                            'max_value' => $crit["max_value"],
                            'condition' => $crit["condition"],
                            'description' => $crit["description"],
                            'property_id' => $prop->id,
                        ]);
                    }
                }

            }

            return $device;
        });

        return response()->json([
            'success' => true,
            'device' => $transaction,
        ]);
    }
    public function show($id)
    {
        return response()->json(Device::with('type.property.criticalValue')->with('owner')->find($id));
    }

    public function uploadIcon(Request $request)
    {
        if (!$request->hasFile('file')) {
            return response()->json([
                'success' => false,
                'error' => 'No File Uploaded',
            ]);
        }

        $file = $request->file('file');

        if (!$file->isValid()) {
            return response()->json([
                'success' => $false,
                'error' => 'File is not valid!',
            ]);
        }

        $extension = explode(".", $file->getClientOriginalName())[1];

        // $path=  $file->storeAs('public/images',$request->get('device') . "." . $extension);

        $device_id = $request->get('device');
        $filename = $device_id . "." . $extension;

        $storagePath = Storage::disk('public')->put('/images', $file);
        $filename = basename($storagePath);

        $type = Type::whereHas('device', function ($q) use ($device_id) {
            $q->where('id', '=', $device_id);
        })->first();
        $type->icon = $filename;
        $type->save();

        $types = Type::where('name', $type->name)->get();

        foreach ($types as $typ) {
            $typ->icon = $filename;
            $typ->save();
        }
        return response()->json([
            'success' => true,
            'name' => $filename,
            'device' => $request->get('device'),
            'type' => $type,
        ]);
    }

    public function delete(Request $request)
    {
        $type = Type::whereHas('device', function ($query) use ($request) {
            $query->where('id', $request->get('id'));
        })->first();
        $type->delete();

        $causer = new User($request->get('currentUser'));
        activity('delete-device')->log($causer->username . ' deleted device with ID: ' . ('"' . $request->get('id') . '"'));
        return;
    }
}
