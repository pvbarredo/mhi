<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Model\Device\Device;
use App\Model\Device\DeviceData;

class MapController extends Controller
{
    public function getDeviceData()
    {

        $devices = Device::with('type')->with('owner')->get();

        foreach ($devices as $device) {
            $device->location = DeviceData::where('device_id', $device->id)->latest('date')->first();
        }

        return $devices;
    }

}
