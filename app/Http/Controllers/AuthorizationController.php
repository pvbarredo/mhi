<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User\User;
use Hash;
use DB;

class AuthorizationController extends Controller
{

    public function login()
    {

        $user = User::with('organizationMember.organization')->with('devices')->where('username', request('username'))->first();

        if(!$user) {
            return response()->json(
                [
                    'message' => 'Username does not exist',
                    'status' => 422
                ], 422
            ); 
        }

        if (!Hash::check(request('password'), $user->password)) {
            return response()->json(
                [
                    'message' => 'Wrong password',
                    'status' => 422
                ], 422
            );
        }

        if(!$user->verified) {
            return response()->json(
                [
                    'message' => 'User is not verified using email yet',
                    'status' => 422
                ], 422
            ); 
        }

        if(!$user->active) {
            return response()->json(
                [
                    'message' => 'Please contact admin for approval',
                    'status' => 422
                ], 422
            ); 
        }


        $data = [
            'grant_type' => 'password',
            'client_id' => env('CLIENT_ID', ''),
            'client_secret' => env('CLIENT_SECRET', ''),
            'username' => request('username'),
            'password' => request('password'),

        ];



        $request = Request::create('/oauth/token', 'POST', $data);

        $response = app()->handle($request);

        if ($response->getStatusCode() != 200) {
                return response()->json(
                    [
                    'message' => 'Token problem',
                    'status' => 422
                    ], 422
                );
        }

        $data = json_decode($response->getContent());


        return response()->json(
            [
                'token' => $data->access_token,
                'user' => $user,
                'status' => 200
            ]
        );
    }

    public function logout()
    {
        $accessToken = auth()->user()->token();

        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update(['revoked'=>true]);

        $accessToken->revoke();    

        return response()->json(['status' => 200]);
    }
    
    public function getCurrentUser()
    {
        return User::with('organizationMember.organization')->with('devices')->find(auth()->user()->id );
    }
}
