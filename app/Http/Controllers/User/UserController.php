<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User\User;
use App\Model\Device\Device;
use App\Events\UserResetPass;
use Illuminate\Support\Facades\Hash;
use Spatie\Activitylog\Models\Activity;
use App\Model\Organization\OrganizationMember;
use DB;

class UserController extends Controller
{

    public function index()
    {
        return User::with('devices')->with('organizationMember.organization')->latest()->paginate(10);
    }

    public function getAllUsers()
    {
        return User::get();
    }


    public function getAllAvailableUsers()
    {
        
        $organizationMember = OrganizationMember::pluck('user_id')->all();
        
        return User::whereNotIn('id', $organizationMember)->where('role','<>','Admin')->get();

    }

    public function getAllForApprovalUsers()
    {

        return User::where('active', false)->paginate(10);

    }

    public function checkUsername(Request $request)
    {

        return User::where('username', $request->get('username'))->first();

    }

    public function checkEmail(Request $request)
    {

        return User::where('email', $request->get('email'))->first();

    }


    public function search(Request $request)
    {
        $user = (new User)->newQuery();
       
        if($request->input('name')) {
            $user->where(
                function ($query) use ($request) {
                    $query->orwhere('firstname', 'like', '%' . $request->input('name'). '%')
                        ->orWhere('middlename', 'like', '%' . $request->input('name'). '%')
                        ->orWhere('surname', 'like', '%' . $request->input('name'). '%')
                        ->orWhere('username', 'like', '%' . $request->input('name'). '%');
                }
            );
        }

        if($request->input('email')) {
            $user->where('email', 'like', '%' . $request->input('email'). '%');
        }

        if($request->input('role')) {
       
            $user->where('role', $request->input('role'));     
        
        }
        
        return $user->paginate(10);
    }

    public function searchActivity(Request $request){
        $activity = (new Activity)->newQuery();

        if($request->input('log_name')){
            $activity->where('log_name','like', '%' . $request->input('log_name'). '%');
        }

        if($request->input('description')){
            $activity->where('description','like', '%' . $request->input('description'). '%');
        }
        
        return $activity->paginate(50);
    }


    public function logActivityUser( $model, $causer, $activity ,$action){
        //message format is '{{causer}} {{action}} {{performedOn}}'
        $message = $causer->username . ' ' . $action . ' ' . 'user: ' . ('"'.$model->username.'"');

        activity($activity)
           ->performedOn($model)
           ->causedBy($causer)
           ->withProperties([
                'firstname' => $model->firstname,
                'middlename' => $model->middlename,
                'surname' => $model->surname,
                'username'=> $model->username,
                'email' => $model->email,
                'role' => $model->role
            ])
           ->log($message);

    }

    public function store(Request $request)
    {

        $transaction = DB::transaction(
            function () use ($request) {

                $user = User::create(
                    [
                    'firstname' => $request->get('user')['firstname'],
                    'middlename' => $request->get('user')['middlename'],
                    'surname' => $request->get('user')['surname'],
                    'username'=> $request->get('user')['username'],
                    'email' => $request->get('user')['email'],
                    'role' => $request->get('user')['role'],
                    'password' => bcrypt('admin123')
                    ]
                );
              
                $causer = new User($request->get('currentUser'));
                $this->logActivityUser($user, $causer, 'create-user' ,'created');

                if($request->get('organization')){
                    OrganizationMember::create([
                        'organization_id' => $request->get('organization')['id'],
                        'user_id' => $user->id
                    ]);
                }
                
                foreach ($request->get('user')['devices'] as $devices) {
                    $device = Device::find($devices["id"]);
                    $device->user_id = $user->id;
                    $device->save();
                   
                }

                return $user;
            }
        );

        return response()->json(
            [
            'success' => true,
            'user' => $transaction
            ]
        );
 
     
    }

    public function signup(Request $request)
    {
        $transaction = DB::transaction(
            function () use ($request) {
                $user = User::create(
                    [
                    'firstname' => $request->get('firstname'),
                    'middlename' => $request->get('middlename'),
                    'surname' => $request->get('surname'),
                    'username'=> $request->get('username'),
                    'email' => $request->get('email'),
                    'role' => $request->get('role'),
                    'password' => bcrypt($request->get('password')),
                    'verified' => true
                    
                    ]
                );
                if($request->get('organization')){
                    OrganizationMember::create([
                        'organization_id' => $request->get('organization')['id'],
                        'user_id' => $user->id
                    ]);
                }

                activity('create-user')->log('System created user: ' . ('"'.$user->username.'"'));
                return $user;
            }
        );

        return response()->json(
            [
            'success' => true,
            'user' => $transaction
            ]
        );
    }

    public function approve(Request $request)
    {
        
        $user = User::find($request->get('user')['id']);
        $user->active = 1;
        $user->save();

        $causer = new User($request->get('currentUser'));
        $this->logActivityUser($user, $causer, 'approve-user' ,'approved');
        return response()->json(
            [
            'success' => true,
            'user' => $user
            ]
        );
    }
    

    public function verify($token)
    {
        $user = User::where('email_token', $token)->first();
        if(isset($user) ) {
            if(!$user->verified) {
                $user->verified = 1;
                $user->save();
                $status = "Your e-mail is verified. You can now login.";

                activity('verify-user')->log('System verified user: ' . ('"'.$user->username.'"'));
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }
 
        return redirect('/login')->with('status', $status);
    }

    public function show($id)
    {
        return response()->json(User::with('organizationMember.organization')->with('devices')->find($id));
    }

    public function requestResetPassword(Request $request)
    {
        $email = $request->get('email');
        $token = bin2hex(random_bytes(32));

        $reset = DB::table('password_resets')->where('email', $email)->first();

        if(!$reset){
            $reset = DB::table('password_resets')
            ->insert(array('email' => $email, 'token' =>  $token));
        }else{
            $reset = DB::table('password_resets')->where('email', $email)
            ->update(array('token' =>  $token));
        }
        
        $request['reset_token'] =  $token;
        event(new UserResetPass($request));
        activity('request-reset-password')->log(('"'.$email.'"') . ' requested to reset password');

        return response()->json(
            [
            'alertType' => 'success',
            'alertMessage' => 'Reset password request was sent to your e-mail. You will be redirected to login page in a few seconds.'
            ]
        );
    }

    public function verifyResetPassword($token){
        $verify = DB::table('password_resets')->where('token', $token)->first();
        $verified = false;
        $email = $verify->email;

        if($verify)
        $verified = true;

        return response()->json(['verified' => $verified, 'email' => $email]);
    }

    public function resetPassword(Request $request){
        
        $reset = DB::table('users')->where('email', $request->get('email'))
            ->update(array('password' => bcrypt($request->get('password'))));
        $reset = DB::table('password_resets')->where('email', $request->get('email'))->delete();

        activity('reset-password')->log(('"'.$request->get('email').'"') . ' reset password');

        return response()->json(
            [
            'alertType' => 'success',
            'alertMessage' => 'Your password has been reset successfully! You will be redirected to login page in a few seconds.'
            ]
        );
    }

    public function update(Request $request, $id) {

        $transaction = DB::transaction(
            function () use ($request, $id) {
                $user = User::find($id);
                $user->firstname = $request->get('user')['firstname'];
                $user->middlename = $request->get('user')['middlename'];
                $user->surname = $request->get('user')['surname'];
                $user->role = $request->get('user')['role'];
                
                $user->devices()->update(['user_id' => null]);
                
                $user->save();

                $organizationMember = OrganizationMember::where('user_id',$user->id)->first();
                if($organizationMember && $organizationMember->organization_id === $request->get('user')['organization']['id']){
               
                }else{
                    if($request->get('user')['role'] != 'Admin'){
                        $organizationMember->organization_id = $request->get('user')['organization']['id'];
                        $organizationMember->approved = false;
                        $organizationMember->save();
                    }
                   
                    
                }

                foreach ($request->get('user')['devices'] as $devices) {
                    $device = Device::find($devices["id"]);
                    $device->user_id = $id;
                    $device->save();
                }

                $causer = new User($request->get('currentUser'));
                $this->logActivityUser($user, $causer, 'update-user' ,'updated');

                return $user;
            }
        );

        return response()->json(
            [
            'success' => true,
            'user' => $transaction
            ]
        );
    }

    public function delete(Request $request){
        $user = User::find($request->get('id'));
        $user->delete();

        $causer = new User($request->get('currentUser'));
        $this->logActivityUser($user, $causer, 'delete-user' ,'deleted');

        return;
    }
    
    public function getUserActivity($id)
    {
        
    }

    public function getAllActivity()
    {
        return Activity::with('causer')->latest()->paginate(50);
    }
}
