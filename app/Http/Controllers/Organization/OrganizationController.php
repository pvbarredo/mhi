<?php

namespace App\Http\Controllers\Organization;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Organization\Organization;
use App\Model\User\User;
use App\Model\Organization\OrganizationMember;
use DB;

class OrganizationController extends Controller
{

    public function logActivityOrganization( $model, $causer, $activity ,$action){
        //message format is '{{causer}} {{action}} {{performedOn}}'
        $message = $causer->username . ' ' . $action . ' ' . ('"'.$model->name.'"') . ' Organization';
        activity($activity)
           ->performedOn($model)
           ->causedBy($causer)
           ->withProperties([
                'name' => $model->name,
                'description' => $model->description,
            ])
           ->log($message);
    }
   
    public function logActivityOrganizationMember( $model, $causer, $activity ,$action){
        //message format is '{{causer}} {{action}} {{performedOn}}'
        $message = $causer->username . ' ' . $action . ' ' . 'user with ID : ' . ('"'.$model->user_id.'"') . ' in organization with ID: '. ('"'.$model->organization_id.'"');
        activity($activity)
           ->performedOn($model)
           ->causedBy($causer)
           ->withProperties([
                'organization_id' => $model->id,
                'user_id' => $model->user_id,
                'approved' => $model->approved,
            ])
           ->log($message);
    }
    
    public function index()
    {
        return Organization::latest()->paginate(10);
    }


    public function checkOrganizationName(Request $request)
    {
       return Organization::where('name', $request->get('name'))->first();
    }

    public function search(Request $request)
    {
        $organization = (new Organization)->newQuery();
       
        if($request->input('name')) {
            $organization->where('name', 'like', '%' . $request->input('name'). '%');
        }

        if($request->input('description')) {
            $organization->where('description', 'like', '%' . $request->input('description'). '%');
        }

        return $organization->paginate(10);
    }

    public function store(Request $request)
    {

       $transaction = DB::transaction(function () use ($request){
            $organization = Organization::create([
                'description' => $request->get('organization')['description'],
                'name' => $request->get('organization')['name']
            ]);
            
            $causer = new User($request->get('currentUser'));
            $this->logActivityOrganization($organization, $causer, 'create-organization' ,'created');

          foreach ($request->get('organization')['members'] as $members) {
              $organizationMembers = OrganizationMember::create([
                  'organization_id' => $organization->id,
                  'user_id' => $members["user"]["id"],
                  'approved' => 0
              ]);    
              $this->logActivityOrganizationMember($organizationMembers, $causer, 'add-organization-member' ,'added');
          }

          return $organization;
      });

        return response()->json([
          'success' => true,
          'organization' => $transaction
      ]); 	

    }

    public function update(Request $request, $id)
    {
    	$transaction = DB::transaction(function () use ($request,$id){
            $organization = Organization::find($id);

            $organization->name = $request->get('organization')['name'];
            $organization->description = $request->get('organization')['description'];
            $organization->members()->delete();
            $organization->save();

            $causer = new User($request->get('currentUser'));
            $this->logActivityOrganization($organization, $causer, 'update-organization' ,'updated');


          foreach ($request->get('organization')['members'] as $members) {
              $organizationMembers = OrganizationMember::create([
                  'organization_id' => $organization->id,
                  'user_id' => $members["user"]["id"],
                  'approved' => 0
                  
              ]);    
              $this->logActivityOrganizationMember($organizationMembers, $causer, 'update-organization-member' ,'updated');
          }

          return $organization;
      });

        return response()->json([
          'success' => true,
          'organization' => $transaction
      ]); 	
       
    }

     public function show($id)
    {
        return response()->json(Organization::with('members.user')->find($id));
    }

    public function delete(Request $request){
        $user = Organization::find($request->get('id'));
        $user->delete();

        $causer = new User($request->get('currentUser'));
        $this->logActivityOrganization($user, $causer, 'delete-organization' ,'deleted');

        return;
    }

    public function getAllForApproval($id){
        $user = User::with('organizationMember')->find($id);

        if($user->role == 'P1'){
            return OrganizationMember::where('approved',0)->where('organization_id', $user['organizationMember']['organization_id'])->with('user')->with('organization')->paginate(10);    
        }
    	return OrganizationMember::where('approved',0)->with('user')->with('organization')->paginate(10);
    }

    public function getAllOrganization(){
    	return Organization::get();
    }

    public function approve(Request $request)
    {
        
        $member = OrganizationMember::find($request->get('member')['id']);
        $member->approved = 1;
        $member->save();

        $causer = new User($request->get('currentUser'));
        $this->logActivityOrganizationMember($member, $causer, 'approve-organization-member' ,'approved');
        return response()->json(
            [
            'success' => true,
            'member' => $member
            ]
        );
    }

}
