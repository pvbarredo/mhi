<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::get('/map', 'Report\MapController@getDeviceData');
Route::get('/deviceData/deleteAll','Device\DeviceDataController@deleteAllData');
Route::get('/deviceData/seedData','Device\DeviceDataController@seedData');
Route::get('/deviceData/addDeviceData','Device\DeviceDataController@addDeviceData');
Route::get('/deviceData/property/{id}','Device\DeviceDataController@getDataUsingPropertyId');

Route::get('/device/getAllDevices','Device\DeviceController@getAllDevices');
Route::get('/device/getAllFreeDevices','Device\DeviceController@getAllFreeDevices');
Route::get('/device/getDeviceType','Device\DeviceController@getDeviceType');
Route::get('/device/getDeviceProperty','Device\DeviceController@getDeviceProperty');
Route::post('/device/delete','Device\DeviceController@delete');
Route::get('/deviceData/dialogGetData','Device\DeviceDataController@dialogGetData');
Route::get('/deviceData/getAllDeviceData','Device\DeviceDataController@getAllDeviceData');


Route::post('/device/getAllDevicesPost','Device\DeviceController@getAllDevicesPost');

Route::post('/device/checkUsername','Device\DeviceController@checkUsername');
Route::post('/device/checkMacAddress','Device\DeviceController@checkMacAddress');
Route::post('/device/checkTypeCode','Device\DeviceController@checkTypeCode');

Route::get('/user/getAllForApprovalUsers','User\UserController@getAllForApprovalUsers');
Route::post('/user/checkUsername','User\UserController@checkUsername');
Route::post('/user/requestResetPassword','User\UserController@requestResetPassword');
Route::post('/user/delete','User\UserController@delete');
Route::get('/user/verifyResetPassword/{token}','User\UserController@verifyResetPassword');
Route::post('/user/resetPassword','User\UserController@resetPassword');
Route::post('/user/checkEmail','User\UserController@checkEmail');
Route::get('/user/verify/{token}', 'User\UserController@verify');
Route::post('/user/signup','User\UserController@signup');
Route::post('/user/approve','User\UserController@approve');
Route::get('/user/getAllUsers','User\UserController@getAllUsers');
Route::get('/user/getAllAvailableUsers','User\UserController@getAllAvailableUsers');
Route::get('/user/getAllOrganization','User\UserController@getAllOrganization');

Route::get('/user/getAllActivity','User\UserController@getAllActivity');
Route::get('/user/getUserActivity','User\UserController@getUserActivity');
Route::post('/user/searchActivity','User\UserController@searchActivity');

Route::post('/device/search','Device\DeviceController@search');
Route::post('/deviceData/search','Device\DeviceDataController@search');
Route::post('/archiveData/search','Device\ArchiveDataController@search');
Route::get('/archive/getArchiveType','Device\ArchiveDataController@getArchiveType');
Route::post('/user/search','User\UserController@search');



Route::post('/device/uploadIcon','Device\DeviceController@uploadIcon');
Route::post('/generatePDF','Report\ReportController@generatePDF');

//organization
Route::get('/organization/{id}/getAllMembers','Organization\OrganizationController@getAllMembers');
Route::get('/organization/getAllForApproval/{id}','Organization\OrganizationController@getAllForApproval');
Route::post('/organization/approve','Organization\OrganizationController@approve');
Route::post('/organization/checkOrganizationName','Organization\OrganizationController@checkOrganizationName');
Route::get('/organization/getAllOrganization','Organization\OrganizationController@getAllOrganization');
Route::post('/organization/search','Organization\OrganizationController@search');
Route::post('/organization/delete','Organization\OrganizationController@delete');


Route::resource('user', 'User\UserController');
Route::resource('organization', 'Organization\OrganizationController');
Route::resource('device', 'Device\DeviceController');
Route::resource('deviceData', 'Device\DeviceDataController');
Route::resource('archiveData', 'Device\ArchiveDataController');


Route::get('/{vue_capture?}', function () {
   return view('mhi');
 })->where('vue_capture', '[\/\w\.-]*');
