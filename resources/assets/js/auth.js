class Auth {
    constructor()
    {
        this.token = window.localStorage.getItem('token');
        let userData = window.localStorage.getItem('user');
        this.user =  (userData) ? JSON.parse(userData) : null;
        
        if(this.token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.token;
            this.getUser();
        }
    }

    login(token, user)
    {
        window.localStorage.setItem('token', token);
        window.localStorage.setItem('user', JSON.stringify(user));

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.token;

        this.token = token;
        this.user = user;
       
        Event.$emit('userLoggedIn');
    }

    logout()
    {
        window.localStorage.removeItem('token');
        window.localStorage.removeItem('user');

        axios.defaults.headers.common['Authorization'] = null;

        this.token = null;
        this.user = null;

    }

    check()
    {
        return !! this.token;
    }
    updateCache()
    {
        window.localStorage.setItem('user', JSON.stringify(this.user));
        window.location.href = "/";
    }
    getUser() 
    {
        var vm = this;
        axios.get('/api/getCurrentUser')
        .then(
            function (user) {
                user.data.organization = (user.data.organization_member) ? user.data.organization_member.organization : {};
                
                var userData = window.localStorage.getItem('user');
                var cacheUser =  (userData) ? JSON.parse(userData) : null;
                vm.user = user.data;

                if(vm.user.role !== cacheUser.role ||  _.isEmpty(vm.user.devices) != _.isEmpty(cacheUser.devices )){
                    
                    vm.updateCache();
                }
              
                
            }
        )
        .catch(
            function (response) {
                console.error(response);
                if(response.response.status === 401) {
                    vm.logout();
                }
            }
        );
    }
}

export default Auth;
