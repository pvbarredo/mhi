import DeviceSearch from '../../components/device/DeviceSearch.vue'
import DeviceForm from '../../components/device/DeviceForm.vue'
import DeviceDataSearch from '../../components/device/DeviceDataSearch.vue'
import ArchiveDataSearch from '../../components/device/ArchiveDataSearch.vue'


export default [
  {
        path:'/device/maintenance/search',
            name: 'device.search',
            component: DeviceSearch,
            meta: { requiresAuth: true }
    },
    {
        path:'/device/maintenance/create',
        name: 'device.create',
        component: DeviceForm,
        meta: { requiresAuth: true }
    },
    {
        path:'/device/maintenance/createOwn',
        name: 'device.createOwn',
        component: DeviceForm,
        meta: { requiresAuth: true }
    },
    {
        path:'/device/maintenance/:id/edit',
        name: 'device.edit',
        component: DeviceForm,
        meta: { requiresAuth: true }
      
    },
    {
        path:'/device/maintenance/data',
        name: 'device.data',
        component: DeviceDataSearch,
        meta: { requiresAuth: true }
      
    },
    {
        path:'/device/maintenance/archive',
        name: 'device.archive',
        component: ArchiveDataSearch,
        meta: { requiresAuth: true }
      
    }


 
    ];
