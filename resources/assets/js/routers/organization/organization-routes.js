import OrganizationSearch from '../../components/organization/OrganizationSearch.vue'
import OrganizationCreate from '../../components/organization/OrganizationForm.vue'
import OrganizationApproval from '../../components/organization/OrganizationApproval.vue'


export default [
    {
        path:'/organization/maintenance/search',
            name: 'organization.search',
            component: OrganizationSearch,
            meta: { requiresAuth: true }
    },

    {
        path:'/organization/maintenance/form',
            name: 'organization.create',
            component: OrganizationCreate,
            meta: { requiresAuth: true }
    },
    {
        path:'/organization/maintenance/:id/edit',
            name: 'organization.edit',
            component: OrganizationCreate,
            meta: { requiresAuth: true }
    },
    {
        path:'/organization/maintenance/approval',
            name: 'organization.approval',
            component: OrganizationApproval,
            meta: { requiresAuth: true }
    }
 
    ];
