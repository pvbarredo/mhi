import UserSearch from '../../components/user/UserSearch.vue'
import UserForm from '../../components/user/UserForm.vue'
import UserApproval from '../../components/user/UserApproval.vue'
import UserSignup from '../../components/user/UserSignup.vue'
import UserRequestResetPassword from '../../components/user/UserRequestResetPassword.vue'
import UserResetPassword from '../../components/user/UserResetPassword.vue'
import UserLogs from '../../components/user/UserLogs.vue'

export default [
  {
        path:'/user/maintenance/search',
            name: 'user.search',
            component: UserSearch,
            meta: { requiresAuth: true }
    },
    {
        path:'/user/maintenance/create',
        name: 'user.create',
        component: UserForm,
        meta: { requiresAuth: true }
    },
    {
        path:'/user/maintenance/:id/edit',
        name: 'user.edit',
        component: UserForm,
        meta: { requiresAuth: true }
      
    },
    {
        path:'/user/maintenance/editProfile',
        name: 'user.editProfile',
        component: UserForm,
        meta: { requiresAuth: true }
    },
    {
        path:'/user/maintenance/approval',
        name: 'user.approval',
        component: UserApproval,
        meta: { requiresAuth: true }
    },
    {
        path:'/user/maintenance/signup',
        name: 'user.signup',
        component: UserSignup
    },
    {
        path:'/user/maintenance/logs',
        name: 'user.logs',
        component: UserLogs
    },
    {
        path:'/user/maintenance/requestResetPassword',
        name: 'user.requestResetPassword',
        component: UserRequestResetPassword
    },
    {
        path:'/resetPassword/:token',
        name: 'user.resetPassword',
        component: UserResetPassword
    }

 
    ];
