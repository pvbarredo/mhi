import Vue from 'vue';
import VueRouter from 'vue-router';

import user_routes from './user/user-routes';
import device_routes from './device/device-routes';
import report_routes from './report/report-routes';
import auth_routes from './auth/auth-routes';
import organization_routes from './organization/organization-routes';

import Home from '../components/Home.vue'

Vue.use(VueRouter)
let routes = [];

routes = routes.concat(user_routes);
routes = routes.concat(device_routes);
routes = routes.concat(report_routes);
routes = routes.concat(auth_routes);
routes = routes.concat(organization_routes);

routes.push({
    path:'/',
        name: 'home',
        component: Home,
        meta: { requiresAuth: true }
});

const router = new VueRouter({
	hashbang: false,
	history: true,
	mode: 'history',
	routes
})


router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {                
        if (!auth.check()) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });

            return;
        }
    }

    next();
})

 export default router;