
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import router from './routers/routes';
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en';
import 'element-ui/lib/theme-chalk/index.css'
import * as VueGoogleMaps from 'vue2-google-maps'
import Alert from './alert';
import App from './components/layout/app'
import Auth from './auth.js'

window.moment = require('moment');
window.auth = new Auth();
window.Event = new Vue;


 axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');


Vue.use(
    VueGoogleMaps, {
        load: {
            key: 'AIzaSyDmb2uLbMK4KtN7KBKJmyoS9suRJxQBAkA',
            v: '3.26',
            libraries: 'places'
        }
    }
);
Vue.use(ElementUI, { locale });
Vue.use(Alert);

import Raven from 'raven-js';
import RavenVue from 'raven-js/plugins/vue';

Raven
    .config('https://aae0f55f50d34df49358decc9a2ee441@sentry.io/1243460')
    .install();

//error notifs
Vue.config.errorHandler = function (err, vm, info) {
    console.error(err);
    Vue.prototype.alertError(err.toString());
    Raven.captureException(err);
};

Vue.config.warnHandler = function (msg, vm, trace) {
    console.error(msg);
    Vue.prototype.alertError(msg);
    Raven.captureException(msg);
};


axios.interceptors.response.use(
    function (response) {
        return response;
    }, function (error) {
        console.error(error);
        Vue.prototype.alertError(error.response.data.message);
        Raven.captureException(error.response.request.responseURL + ' ' + error.message);
        return Promise.reject(error);
    }
);

const app = new Vue(
    {
        el: '#app',
        router,
        render: app => app(App)
    }
);

