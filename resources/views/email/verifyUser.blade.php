<!DOCTYPE html>
<html>
<head>
    <title>Verify your account</title>
</head>
<body style="font-family: Roboto, Century Gothic; color: #2b522c;" >
    <div style="margin:0 auto; width:75%; text-align:left">
        <div style="border: 1px solid #ebeef5; border-radius: 5px;">
        <div style="padding: 15px;">
			<a href="{{url('/')}}">
				<img style="display: block; margin-left: auto; margin-right: auto; max-width: 100%; height: auto;" 
				src="{{asset('/img/logo-dark.png')}}" />
			</a>
		</div>
        <div style="background-color: #f4f6f0; padding: 15px;">
			<h2>Hello, {{$user['firstname']}}!</h2>
			<p>Thank you for signing up to MHI, to get started you will need to verify your email address. To do so, please click the link below:
				<br>
				<a href="{{url('user/verify', $user->email_token) }}">Verify My Email Address</a>
			</p>

			<br>
			<p>All the best,<br>
			MHI Team</p>
        </div>
        </div>
    </div>
</body>
</html>