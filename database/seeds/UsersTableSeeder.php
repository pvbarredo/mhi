<?php

use Illuminate\Database\Seeder;
use App\Model\User\User;

class UsersTableSeeder extends Seeder
{
    public function logActivity( $model){
        activity('create-user')
           ->performedOn($model)
           ->causedBy(User::get()->first())
           ->withProperties([
                'firstname' => $model->firstname,
                'middlename' => $model->middlename,
                'surname' => $model->surname,
                'email' => $model->email,
                'username' => $model->username,
                'password' =>$model->password,
                'role' => $model->role,
                'active' => $model->active,
                'verified' => $model->verified
            ])
           ->log('Created User '.$model->username.' by Seeder');

    }
    
    public function run()
    {

        $faker = Faker\Factory::create();

        $user = User::create(
            [
            'firstname' => 'Veronica',
            'middlename' => 'Del Puerto',
            'surname' => 'Gubatan',
            'email' => 'vdgubatan@gmail.com',
            'username' => 'admin',
            'password' => bcrypt('admin123'),
            'role' => 'Admin',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Lois',
            'middlename' => 'Murillio',
            'surname' => 'Igni',
            'email' => 'imigni@gmail.com',
            'username' => 'Ligni',
            'password' => bcrypt('admin123'),
            'role' => 'P1',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Sweeselle',
            'middlename' => 'Montanez',
            'surname' => 'Cardama',
            'email' => 'carda@gmail.com',
            'username' => 'scardama',
            'password' => bcrypt('admin123'),
            'role' => 'P1',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Jessie Jay',
            'middlename' => 'Castillo',
            'surname' => 'Salvador',
            'email' => 'jay09@gmail.com',
            'username' => 'jsalvador',
            'password' => bcrypt('admin123'),
            'role' => 'P1',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Francoyze',
            'middlename' => 'Lamsen',
            'surname' => 'Quezada',
            'email' => 'fran@gmail.com',
            'username' => 'fquezada',
            'password' => bcrypt('admin123'),
            'role' => 'P1',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Angelo',
            'middlename' => 'Madronio',
            'surname' => 'Zamora',
            'email' => 'angelo@gmail.com',
            'username' => 'azamora',
            'password' => bcrypt('admin123'),
            'role' => 'P2',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Mickoe',
            'middlename' => 'Gomez',
            'surname' => 'Domingo',
            'email' => 'mdom@gmail.com',
            'username' => 'mdomingo',
            'password' => bcrypt('admin123'),
            'role' => 'P2',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Jazmine',
            'middlename' => 'Cruz',
            'surname' => 'Calma',
            'email' => 'calma@gmail.com',
            'username' => 'jcalma',
            'password' => bcrypt('admin123'),
            'role' => 'P2',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Jasmin',
            'middlename' => 'Pama',
            'surname' => 'Veloria',
            'email' => 'cyrusalexander@gmail.com',
            'username' => 'hasmin',
            'password' => bcrypt('admin123'),
            'role' => 'P3',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Jett Arianne',
            'middlename' => 'Medrano',
            'surname' => 'Hidalgo',
            'email' => 'condelpuerto@gmail.com',
            'username' => 'jetty',
            'password' => bcrypt('admin123'),
            'role' => 'P3',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user =  User::create(
            [
            'firstname' => 'Eric',
            'middlename' => 'Del Puero',
            'surname' => 'Gubatan',
            'email' => 'exosen@gmail.com',
            'username' => 'exosen',
            'password' => bcrypt('admin123'),
            'role' => 'P3',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Marleon',
            'middlename' => 'Panganiban',
            'surname' => 'Escobar',
            'email' => 'ssmanongsong@gmail.com',
            'username' => 'escobeer',
            'password' => bcrypt('admin123'),
            'role' => 'P2',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Cyrus',
            'middlename' => 'Murillio',
            'surname' => 'Del Puerto',
            'email' => 'Cyrus@gmail.com',
            'username' => 'cypogi',
            'password' => bcrypt('admin123'),
            'role' => 'P3',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user =  User::create(
            [
            'firstname' => 'Karlo',
            'middlename' => 'Dominguez',
            'surname' => 'Inting',
            'email' => 'kdinting@gmail.com',
            'username' => 'kdinting',
            'password' => bcrypt('admin123'),
            'role' => 'P3',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Denzel',
            'middlename' => 'Santos',
            'surname' => 'Pituc',
            'email' => 'denzelpituc@gmail.com',
            'username' => 'denzpituc',
            'password' => bcrypt('admin123'),
            'role' => 'P2',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Dayle',
            'middlename' => 'Jose',
            'surname' => 'Igno',
            'email' => 'dayleigno@gmail.com',
            'username' => 'digno',
            'password' => bcrypt('admin123'),
            'role' => 'P2',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Christian',
            'middlename' => 'Cabral',
            'surname' => 'Banastao',
            'email' => 'xtian@gmail.com',
            'username' => 'xtian',
            'password' => bcrypt('admin123'),
            'role' => 'P3',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Paolo',
            'middlename' => 'Ramos',
            'surname' => 'Domondon',
            'email' => 'paodion@gmail.com',
            'username' => 'paodion',
            'password' => bcrypt('admin123'),
            'role' => 'P1',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Sarah',
            'middlename' => 'Dela Torre',
            'surname' => 'Llanza',
            'email' => 'slanza@gmail.com',
            'username' => 'sllanza',
            'password' => bcrypt('admin123'),
            'role' => 'P1',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        $user = User::create(
            [
            'firstname' => 'Marsielle',
            'middlename' => 'Hernandez',
            'surname' => 'Salgado',
            'email' => 'marshal@gmail.com',
            'username' => 'marshal',
            'password' => bcrypt('admin123'),
            'role' => 'P3',
            'active' => true,
            'verified' => true
            ]
        );
        $this->logActivity($user);
        
    }
}
