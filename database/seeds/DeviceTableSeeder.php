<?php

use Illuminate\Database\Seeder;
use App\Model\Device\Device;
use App\Model\Device\Type;
use App\Model\Device\Property;
use App\Model\Device\CriticalValue;
use App\Model\User\User;

class DeviceTableSeeder extends Seeder
{
    public function logDeviceActivity( $model){
        activity('create-device')
           ->performedOn($model)
           ->causedBy(User::get()->first())
           ->withProperties([
                'name' =>  $model->name,
                'username' =>  $model->username,
                'mac_address' =>  $model->mac_address,
                'user_id' => $model->user_id,
                'type_id' =>  $model->type_id,
                'description'=>  $model->description
            ])
           ->log('Created Device by Seeder');

    }
    
    public function logTypeActivity( $model){
        activity('create-type')
           ->performedOn($model)
           ->causedBy(User::get()->first())
           ->withProperties([
                'code' =>  $model->code,
                'name' =>  $model->name
            ])
           ->log('Created by Seeder');

    }

    public function logCriticalValueActivity( $model){
        activity('create-criticalValue')
           ->performedOn($model)
           ->causedBy(User::get()->first())
           ->withProperties([
                'property_id' =>  $model->property_id,
                'min_value' =>  $model->min_value,
                'max_value' =>  $model->max_value,
                'description' =>  $model->description,
                'condition' =>  $model->condition
            ])
           ->log('Created by Seeder');

    }

    public function logPropertyActivity( $model){
        activity('create-propertyValue')
           ->performedOn($model)
           ->causedBy(User::get()->first())
           ->withProperties([
                'code'=>  $model->code,
               'name' =>  $model->name,
                'unit' =>  $model->unit,
               'type_id' => $model->type_id
            ])
           ->log('Created by Seeder');

    }

    public function run()
    {
        $type1 = Type::create(
            [
            'code' => 'Water3',
            'name' => 'Water',
            ]
        );

        $this->logTypeActivity($type1);

        $device1 =  Device::create(
            [
            'name' => 'Device 1',
            'username' => 'device1',
            'mac_address' => '5b:5n:po:Gi:Pe:Te:r1',
            // 'password' => bcrypt('admin125'),
            'user_id' => '2',
            'type_id' => $type1->id,
            'description'=> 'Properties in water wherein fishes can live'

            ]
        );

        $this->logDeviceActivity($device1);

        $type1_property1 = Property::create(
            [
               'code'=> 'DO',
               'name' => 'Dissolved Oxygen',
                'unit' => 'PPM',
               'type_id' => $type1->id
            ]
        );

        $this->logPropertyActivity($type1_property1);

        $property1_criticalValue1 = CriticalValue::create(
            [
               'property_id' => $type1_property1->id,
               'min_value' => 3.0,
                'max_value' => 0,
                'description' => 'Fish cant live in this water',
                'condition' => 'Exact'

            ]
        );

        $this->logCriticalValueActivity($property1_criticalValue1);

        $property1_criticalValue2 = CriticalValue::create(
            [
            'property_id' => $type1_property1->id,
            'min_value' => 6.0,
            'max_value' => 9.0,
            'description' => 'Fish can survive in this water',
            'condition' => 'Range'

            ]
        );

        $this->logCriticalValueActivity($property1_criticalValue2);

        $type1_property2 = Property::create(
            [
            'code'=> 'NA',
            'name' => 'Sodium',
            'unit' => 'mg/L',
            'type_id' => $type1->id
            ]
        );

        $this->logTypeActivity($type1_property2);

        $property1_criticalValue2 = CriticalValue::create(
            [
            'property_id' => $type1_property2->id,
            'min_value' => 400,
            'max_value' => 0,
            'description' => 'Stressful to fishes',
            'condition' => 'Exact'

            ]
        );

        $this->logCriticalValueActivity($property1_criticalValue2);

        //================================================================

        $type2 = Type::create(
            [
            'code' => 'Air',
            'name' => 'Air',
            ]
        );

        $this->logTypeActivity($type2);

        $device2 =  Device::create(
            [
            'name' => 'Device 2',
            'username' => 'device2',
            'mac_address' => '5b:5n:p:125:52:11',
            // 'password' => bcrypt('admin125'),
            'user_id' => '3',
            'type_id' => $type2->id,
            'description'=> 'Level of CO2 harmful to human'

            ]
        );

        $this->logDeviceActivity($device2);

        $type2_property1 = Property::create(
            [
            'code'=> 'CO2',
            'name' => 'Carbon Dioxide',
            'unit' => 'PPM',
            'type_id' => $type2->id
            ]
        );

        $this->logPropertyActivity($type2_property1);

        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type2_property1->id,
            'min_value' => 400,
            'max_value' => 0,
            'description' => 'Harmful to human',
            'condition' => 'Above'

            ]
        );
        
        $this->logCriticalValueActivity($property1_criticalValue1);

        //===========================================================
        $type3 = Type::create(
            [
            'code' => 'Soil',
            'name' => 'Soil',
            ]
        );

        $this->logDeviceActivity($type3);

        $device3 =  Device::create(
            [
            'name' => 'Device 3',
            'username' => 'device3',
            'mac_address' => '5b:5n:12:5a:p:125:52:11',
            // 'password' => bcrypt('admin125'),
            'user_id' => '4',
            'type_id' => $type3->id,
            'description'=> 'Level of K Good for planting'

            ]
        );

        $this->logDeviceActivity($device3);

        $type3_property1 = Property::create(
            [
            'code'=> 'POT',
            'name' => 'Potassium',
            'unit' => 'PPM',
            'type_id' => $type3->id
            ]
        );

        $this->logPropertyActivity($type3_property1);

        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type3_property1->id,
            'min_value' => 20000,
            'max_value' => 0,
            'description' => 'Good for plants',
            'condition' => 'Exact'

            ]
        );

        $this->logCriticalValueActivity($property1_criticalValue1);

        //===========================================================

        $type4 = Type::create(
            [
            'code' => 'Air18',
            'name' => 'Air',
            ]
        );
        $this->logTypeActivity($type4);

        $device4 =  Device::create(
            [
            'name' => 'Device 4',
            'username' => 'device4',
            'mac_address' => '5b:5n:12:5a:p:1asd:325:52:11',
            // 'password' => bcrypt('admin125'),
            'user_id' => '5',
            'type_id' => $type4->id,
            'description'=> 'Cause of Acid Rain'

            ]
        );
        
        $this->logDeviceActivity($device4);

        $type4_property1 = Property::create(
            [
            'code'=> 'PH',
            'name' => 'PH',
            'unit' => 'PPM',
            'type_id' => $type4->id
            ]
        );

        $this->logPropertyActivity($type4_property1);

        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type4_property1->id,
            'min_value' => 5,
            'max_value' => 0,
            'description' => 'Prone to acid rain',
            'condition' => 'Above'

            ]
        );
        //===========================================================
        
        $this->logCriticalValueActivity($property1_criticalValue1);

        $type5 = Type::create(
            [
            'code' => 'Air2',
            'name' => 'Air',
            ]
        );

        $this->logTypeActivity($type5);

        $device5 =  Device::create(
            [
            'name' => 'Device 5',
            'username' => 'device5',
            'mac_address' => '5b:5n:12:5a:ps2:1asd:325:52:11',
            // 'password' => bcrypt('admin125'),
            'user_id' => '6',
            'type_id' => $type5->id,
            'description'=> 'O3 harmful to human'

            ]
        );
        $this->logDeviceActivity($device5);
        $type5_property1 = Property::create(
            [
            'code'=> 'OZ',
            'name' => 'Ozone',
            'unit' => 'ppb',
            'type_id' => $type5->id
            ]
        );
        $this->logPropertyActivity($type5_property1);
        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type5_property1->id,
            'min_value' => 4.0,
            'max_value' => 0,
            'description' => 'Harmful to human',
            'condition' => 'Below'

            ]
        );
        $this->logCriticalValueActivity($property1_criticalValue1);
        //===========================================================

        $type = Type::create(
            [
            'code' => 'Water1',
            'name' => 'Water',
            ]
        );
        $this->logTypeActivity($type);
        $device =  Device::create(
            [
            'name' => 'Device 6',
            'username' => 'device6',
            'mac_address' => '5b:5n:12:5a:ps2:1asd:325:52:13241',
            // 'password' => bcrypt('admin125'),
            'user_id' => '7',
            'type_id' => $type->id,
            'description'=> 'Device'

            ]
        );
        $this->logDeviceActivity($device);
        $type_property1 = Property::create(
            [
            'code'=> 'Temp',
            'name' => 'Temperature',
            'unit' => 'Celsius',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property1);
        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property1->id,
            'min_value' => 40,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Above'

            ]
        );
        $this->logCriticalValueActivity($property1_criticalValue1);
        //===========================================================

        $type = Type::create(
            [
            'code' => 'Air1',
            'name' => 'Air',
            ]
        );
        $this->logTypeActivity($type);
        $device =  Device::create(
            [
            'name' => 'Device 7',
            'username' => 'device76',
            'mac_address' => '5b:57n:12:5a:ps2:1asd:325:52:13241',
            // 'password' => bcrypt('admin125'),
            'user_id' => '8',
            'type_id' => $type->id,
            'description'=> 'Device'

            ]
        );
        $this->logDeviceActivity($device);
        $type_property1 = Property::create(
            [
            'code'=> 'Humidity',
            'name' => 'Humidity',
            'unit' => 'Percentage',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property1);
        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property1->id,
            'min_value' => 90,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Above'

            ]
        );
        $this->logCriticalValueActivity($property1_criticalValue1);
        $type_property2 = Property::create(
            [
            'code'=> 'Temperature2',
            'name' => 'Temperature',
            'unit' => 'Celsius',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property2);
        $property2_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property2->id,
            'min_value' => 40,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Above'

            ]
        );
        $this->logCriticalValueActivity($property2_criticalValue1);
        //===========================================================

        $type = Type::create(
            [
            'code' => 'Water2',
            'name' => 'Water',
            ]
        );
        $this->logTypeActivity($type);
        $device =  Device::create(
            [
            'name' => 'Device 8',
            'username' => 'device8',
            'mac_address' => '5b:57n:128:5a:ps2:1asd:325:52:13241',
            // 'password' => bcrypt('admin125'),
            'user_id' => '9',
            'type_id' => $type->id,
            'description'=> 'Device'

            ]
        );
        $this->logDeviceActivity($device);
        $type_property1 = Property::create(
            [
            'code'=> 'Turbidity',
            'name' => 'Turbidity',
            'unit' => 'NTU',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property1);
        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property1->id,
            'min_value' => 5,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Above'

            ]
        );
        $this->logCriticalValueActivity($property1_criticalValue1);
        $type_property2 = Property::create(
            [
            'code'=> 'PH level',
            'name' => 'PH Level',
            'unit' => 'PH Scale',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property2);
        $property2_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property2->id,
            'min_value' => 6,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Below'

            ]
        );
        $this->logCriticalValueActivity($property2_criticalValue1);
        //===========================================================

        $type = Type::create(
            [
            'code' => 'Light',
            'name' => 'Light',
            ]
        );
        $this->logTypeActivity($type);
        $device =  Device::create(
            [
            'name' => 'Device 9',
            'username' => 'device9',
            'mac_address' => '5b:57nwerewr123:128:5a:ps2:1asd:325:52:13241',
            // 'password' => bcrypt('admin125'),
            'user_id' => '10',
            'type_id' => $type->id,
            'description'=> 'Device'

            ]
        );
        $this->logDeviceActivity($device);
        $type_property1 = Property::create(
            [
            'code'=> 'LightIntensity',
            'name' => 'LightIntensity',
            'unit' => 'Lumens',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property1);
        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property1->id,
            'min_value' => 220,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Above'

            ]
        );

        $this->logCriticalValueActivity($property1_criticalValue1);
        //===========================================================

        $type = Type::create(
            [
            'code' => 'Sound',
            'name' => 'Sound',
            ]
        );
        $this->logTypeActivity($type);
        $device =  Device::create(
            [
            'name' => 'Device 10',
            'username' => 'device10',
            'mac_address' => '5b:57n11023:128:5a:ps2:1asd:325:52:13241',
            // 'password' => bcrypt('admin125'),
            'user_id' => '11',
            'type_id' => $type->id,
            'description'=> 'Device'

            ]
        );
        $this->logDeviceActivity($device);
        $type_property1 = Property::create(
            [
            'code'=> 'Sound',
            'name' => 'Accoustic Sound',
            'unit' => 'Db',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property1);
        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property1->id,
            'min_value' => 85,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Above'

            ]
        );
        $this->logCriticalValueActivity($property1_criticalValue1);
        //===========================================================

        $type = Type::create(
            [
            'code' => 'Air23',
            'name' => 'Air',
            ]
        );
        $this->logTypeActivity($type);
        $device =  Device::create(
            [
            'name' => 'Device 11',
            'username' => 'device11',
            'mac_address' => '5b:5712n11023:128:5a:ps2:1asd:325:52:13241',
            // 'password' => bcrypt('admin125'),
            'user_id' => '12',
            'type_id' => $type->id,
            'description'=> 'Device'

            ]
        );
        $this->logDeviceActivity($device);
        $type_property1 = Property::create(
            [
            'code'=> 'Pressur',
            'name' => 'Pressure',
            'unit' => 'Atm',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property1);
        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property1->id,
            'min_value' => 2,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Above'

            ]
        );
        $this->logCriticalValueActivity($property1_criticalValue1);
        //===========================================================

        $type = Type::create(
            [
            'code' => 'Enery',
            'name' => 'Energy',
            ]
        );
        $this->logTypeActivity($type);
        $device =  Device::create(
            [
            'name' => 'Device 12',
            'username' => 'device12',
            'mac_address' => '5b:52712n11023:128:5a:p12s2:1asd:325:52:13241',
            // 'password' => bcrypt('admin125'),
            'user_id' => '13',
            'type_id' => $type->id,
            'description'=> 'Device'

            ]
        );
        $this->logDeviceActivity($device);
        $type_property1 = Property::create(
            [
            'code'=> 'Radiation',
            'name' => 'Radiation',
            'unit' => 'Siever',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property1);
        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property1->id,
            'min_value' => 20,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Above'

            ]
        );
        $this->logCriticalValueActivity($property1_criticalValue1);
        //===========================================================

        $type = Type::create(
            [
            'code' => 'Air90',
            'name' => 'Air',
            ]
        );
        $this->logTypeActivity($type);
        $device =  Device::create(
            [
            'name' => 'Device 13',
            'username' => 'device13',
            'mac_address' => '5b:523712n11023:128:5a:ps2:1asd:325:52:13241',
            // 'password' => bcrypt('admin125'),
            'user_id' => '14',
            'type_id' => $type->id,
            'description'=> 'Device'

            ]
        );
        $this->logDeviceActivity($device);
        $type_property1 = Property::create(
            [
            'code'=> 'ParticulateMatter',
            'name' => 'Particulate',
            'unit' => 'PM',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property1);
        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property1->id,
            'min_value' => 10,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Below'

            ]
        );
        $this->logCriticalValueActivity($property1_criticalValue1);
        //===========================================================

        $type = Type::create(
            [
            'code' => 'Air902',
            'name' => 'Air',
            ]
        );
        $this->logTypeActivity($type);
        $device =  Device::create(
            [
            'name' => 'Device 14',
            'username' => 'device14',
            'mac_address' => '5b:5243712n11023:128:5a:ps2:1asd:325:52:13241',
            // 'password' => bcrypt('admin125'),
            'user_id' => '15',
            'type_id' => $type->id,
            'description'=> 'Device'

            ]
        );
        $this->logDeviceActivity($device);
        $type_property1 = Property::create(
            [
            'code'=> 'Air566',
            'name' => 'Wind Speed',
            'unit' => 'km/h',
            'type_id' => $type->id
            ]
        );
        $this->logPropertyActivity($type_property1);
        $property1_criticalValue1 = CriticalValue::create(
            [
            'property_id' => $type_property1->id,
            'min_value' => 60,
            'max_value' => 0,
            'description' => 'Harmful to ewan , wala sa excel eh',
            'condition' => 'Above'

            ]
        );
        $this->logCriticalValueActivity($property1_criticalValue1);

    }
}
