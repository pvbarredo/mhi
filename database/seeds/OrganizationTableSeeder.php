<?php

use Illuminate\Database\Seeder;
use App\Model\Organization\Organization;
use App\Model\Organization\OrganizationMember;
use App\Model\User\User;

class OrganizationTableSeeder extends Seeder
{
   public function logActivity( $model){
        activity('create-organization')
           ->performedOn($model)
           ->causedBy(User::get()->first())
           ->withProperties([
                'name' => $model->name,
                'description' => $model->description
            ])
           ->log('Created Organization by Seeder');

    }

    public function logOrganizationMember( $model){
        activity('create-organization-member')
           ->performedOn($model)
           ->causedBy(User::get()->first())
           ->withProperties([
                'organization_id' => $model->name,
                'user_id' => $model->name,
                'approved' => $model->name
            ])
           ->log('Created Organization member by Seeder');

    }

    
    public function run()
    {
        $ust = Organization::create(
            [
            'name' => 'UST',
            'description' => 'UST',
            ]
        );

        $this->logActivity($ust);
        
        $apc =  Organization::create(
            [
            'name' => 'APC',
            'description' => 'APC',
            ]
        );
        $this->logActivity($apc);
        $nu = Organization::create(
            [
            'name' => 'NU',
            'description' => 'NU',
            ]
        );
        $this->logActivity($nu);
        $feu = Organization::create(
            [
            'name' => 'FEU',
            'description' => 'FEU',
            ]
        );
        $this->logActivity($feu);
        $pup =  Organization::create(
            [
            'name' => 'PUP',
            'description' => 'PUP',
            ]
        );
        $this->logActivity($pup);


        $organizationMember = OrganizationMember::create([
        	'organization_id' => $ust->id,
        	'user_id' => 2,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $ust->id,
        	'user_id' => 6,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $ust->id,
        	'user_id' => 7,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $apc->id,
        	'user_id' => 3,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $apc->id,
        	'user_id' => 8,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $apc->id,
        	'user_id' => 12,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $nu->id,
        	'user_id' => 4,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $nu->id,
        	'user_id' => 15,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $nu->id,
        	'user_id' => 16,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $pup->id,
        	'user_id' => 5,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $pup->id,
        	'user_id' => 9,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $pup->id,
        	'user_id' => 10,
        	'approved' => 1
        ]);

        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $feu->id,
        	'user_id' => 18,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $feu->id,
        	'user_id' => 11,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $feu->id,
        	'user_id' => 13,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
        $organizationMember = OrganizationMember::create([
        	'organization_id' => $ust->id,
        	'user_id' => 19,
        	'approved' => 1
        ]);
        $this->logOrganizationMember($organizationMember);
    }
}
