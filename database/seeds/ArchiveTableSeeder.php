<?php

use Illuminate\Database\Seeder;
use App\Model\Device\ArchiveData;
use App\Model\User\User;

class ArchiveTableSeeder extends Seeder
{
    public function logActivity( $model){
        activity('create-archive-data')
           ->performedOn($model)
           ->causedBy(User::get()->first())
           ->withProperties([
                'device_id'=> $model->device_id,
                'property_id' => $model->property_id,
                'value' => $model->value,
                'latitude' => $model->latitude,
                'longitude' => $model->longitude
            ])
           ->log('Created Archive data by Seeder');

    }

   public function run()
    {
        $faker = Faker\Factory::create();
        
         $data = ArchiveData::create(
            [
            'device_id'=> '1',
            'property_id' => '1',
            'value' => null,
            'latitude' => null,
            'longitude' => null
            ]
        );

        $this->logActivity($data);

        $data = ArchiveData::create(
            [
            'device_id'=> '3',
            'property_id' => '4',
            'value' => 20000,
            'latitude' => null,
            'longitude' => '120.961203'
            ]
        );
        
        $this->logActivity($data);

        $data = ArchiveData::create(
            [
            'device_id'=> '4',
            'property_id' => '5',
            'value' => null,
            'latitude' => '14.509547',
            'longitude' => null
            ]
        );
        $this->logActivity($data);
        $data = ArchiveData::create(
            [
            'device_id'=> '5',
            'property_id' => '6',
            'value' => 2,
            'latitude' => null,
            'longitude' => '121.030798'
            ]
        );
     
        $this->logActivity($data);

        foreach (range(1, 20) as $index) {
            $data = ArchiveData::create(
                [
                'device_id'=> '1',
                'property_id' => '1',
                'value' =>$faker->numberBetween(1, 10) ,
                'latitude' => '14.473604',
                'longitude' => null,
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
            $this->logActivity($data);
            
        }
        foreach (range(1, 20) as $index) {
           $data =  ArchiveData::create(
                [
                'device_id' => '1',
                'property_id' => '2',
                'latitude' => '14.550829',
                'longitude' => '121.019624',
                'value' => null,
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
           $this->logActivity($data);
        }

        foreach (range(1, 20) as $index) {
            $data = ArchiveData::create(
                [
                'device_id' => '2',
                'property_id' => '3',
                'latitude' => '14.547756',
                'longitude' => null,
                'value' => $faker->numberBetween(300, 600),
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
            $this->logActivity($data);
        }

        foreach (range(1, 20) as $index) {
            $data = ArchiveData::create(
                [
                'device_id' => '3',
                'property_id' => '4',
                'latitude' => '14.424344 ',
                'longitude' => null,
                'value' => $faker->numberBetween(15000, 30000),
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
            $this->logActivity($data);
        }
        
        foreach (range(1, 20) as $index) {
            $data = ArchiveData::create(
                [
                'device_id' => '4',
                'property_id' => '5',
                'latitude' => '14.509547',
                'longitude' => null,
                'value' => null,
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
            $this->logActivity($data);
        }
        
        foreach (range(1, 20) as $index) {
            $data = ArchiveData::create(
                [
                'device_id' => '5',
                'property_id' => '6',
                'latitude' => null,
                'longitude' => '121.030798',
                'value' => $faker->numberBetween(1, 10),
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
            $this->logActivity($data);
        }


    }
}
