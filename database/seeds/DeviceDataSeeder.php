<?php

use Illuminate\Database\Seeder;
use App\Model\Device\DeviceData;
use App\Model\User\User;

class DeviceDataSeeder extends Seeder
{
   public function logActivity( $model){
        activity('create-device-data')
           ->performedOn($model)
           ->causedBy(User::get()->first())
           ->withProperties([
                'device_id'=> $model->device_id,
                'property_id' => $model->property_id,
                'value' => $model->value,
                'latitude' => $model->latitude,
                'longitude' => $model->longitude
            ])
           ->log('Created Device Data by Seeder');

    }

    public function run()
    {
        $faker = Faker\Factory::create();
        
         $data = DeviceData::create(
            [
            'device_id'=> '1',
            'property_id' => '1',
            'value' => 4,
            'latitude' => '14.473604',
            'longitude' => '121.080075'
            ]
        );

        $this->logActivity($data);

        $data = DeviceData::create(
            [
            'device_id'=> '3',
            'property_id' => '4',
            'value' => 20000,
            'latitude' => '14.424344',
            'longitude' => '120.961203'
            ]
        );
        
        $this->logActivity($data);

        $data = DeviceData::create(
            [
            'device_id'=> '4',
            'property_id' => '5',
            'value' => 6,
            'latitude' => '14.509547',
            'longitude' => '121.050296'
            ]
        );
        $this->logActivity($data);
        $data = DeviceData::create(
            [
            'device_id'=> '5',
            'property_id' => '6',
            'value' => 2,
            'latitude' => '14.663343',
            'longitude' => '121.030798'
            ]
        );
     
        $this->logActivity($data);

        foreach (range(1, 20) as $index) {
            $data = DeviceData::create(
                [
                'device_id'=> '1',
                'property_id' => '1',
                'value' =>$faker->numberBetween(1, 10) ,
                'latitude' => '14.473604',
                'longitude' => '121.080075',
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
            $this->logActivity($data);
            
        }
        foreach (range(1, 20) as $index) {
           $data =  DeviceData::create(
                [
                'device_id' => '1',
                'property_id' => '2',
                'latitude' => '14.550829',
                'longitude' => '121.019624',
                'value' => $faker->numberBetween(300, 600),
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
           $this->logActivity($data);
        }

        foreach (range(1, 20) as $index) {
            $data = DeviceData::create(
                [
                'device_id' => '2',
                'property_id' => '3',
                'latitude' => '14.547756',
                'longitude' => '120.986065',
                'value' => $faker->numberBetween(300, 600),
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
            $this->logActivity($data);
        }

        foreach (range(1, 20) as $index) {
            $data = DeviceData::create(
                [
                'device_id' => '3',
                'property_id' => '4',
                'latitude' => '14.424344 ',
                'longitude' => '120.961203',
                'value' => $faker->numberBetween(15000, 30000),
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
            $this->logActivity($data);
        }
        
        foreach (range(1, 20) as $index) {
            $data = DeviceData::create(
                [
                'device_id' => '4',
                'property_id' => '5',
                'latitude' => '14.509547',
                'longitude' => '121.050296',
                'value' => $faker->numberBetween(1, 10),
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
            $this->logActivity($data);
        }
        
        foreach (range(1, 20) as $index) {
            $data = DeviceData::create(
                [
                'device_id' => '5',
                'property_id' => '6',
                'latitude' => '14.663343',
                'longitude' => '121.030798',
                'value' => $faker->numberBetween(1, 10),
                'date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = null)
                ]
            );
            $this->logActivity($data);
        }


    }
}
