<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateArchiveDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'archive_data', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('device_id')->unsigned();
                $table->integer('property_id')->unsigned();
                $table->integer('value')->nullable();
                $table->string('latitude')->nullable();
                $table->string('longitude')->nullable();
                $table->dateTime('date')->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archive_data');
    }
}
